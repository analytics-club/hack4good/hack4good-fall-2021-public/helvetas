import sys
sys.path.append("src")
from pipeline import HelvetasPipeline
from wordcloud import WordCloud
import dash
from dash import dcc
from dash import html
from dash.dependencies import Input, Output, State
import base64
import io
from io import BytesIO
import dash_table
import plotly.express as px
import pandas as pd
import time
import csv
import plotly.graph_objs as go
import random

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
# app = dash.Dash(__name__)

pipeline = HelvetasPipeline()

shouldTranslate = False

logo_image = "helvetas_logo.png"
logo_base64 = base64.b64encode(open(logo_image, 'rb').read()).decode('ascii')

logo_image = "ethz_logo.png"
ethz_base64 = base64.b64encode(open(logo_image, 'rb').read()).decode('ascii')

app.layout = html.Div([
    html.Div(children=[
        html.Div(html.Img(src='data:image/png;base64,{}'.format(logo_base64), style={'height':'125px'}), style={'display':'inline-block'} ),
        html.Div(html.H1("Helvetas Progress File Analysis Tool"), style={'textAlign':'center','display':'inline-block'}),
        html.Div(html.Img(src='data:image/png;base64,{}'.format(ethz_base64), style={'height':'125px'}),style={'display':'inline-block','text-align':'right'})
    ]),
    dcc.Tabs(id='tabs', value='tabs-1', children=[
        dcc.Tab(label='Data', value='tabs-1', children=[
            html.Div([
                html.Div(
                    dcc.Upload(
                        id='datafile-upload',
                        children=html.Div([
                            'Drag and Drop or ',
                            html.A('Select Files')
                        ]),
                        multiple=True,
                        style={                            
                            'textAlign': 'center'
                        }
                    ), 
                    style={
                        'width': '49%', 'lineHeight': '60px',
                        'borderWidth': '1px', 'borderStyle': 'dashed',
                        'borderRadius': '5px', 'textAlign': 'center', 'margin': '10px',
                        'display':'inline-block'
                    }
                ),
                html.Div(
                    id='filelist-string', style={'width': '49%', 'display':'inline-block'}
                )
            ]),
            html.Div(
                id='table-and-text-fields',
                children=[                
                    html.Div([
                        html.Div(
                            html.Button("Process Data", id='load-data-button', n_clicks=0), style={'display':'inline-block'}
                        ),        
                            html.Div(dcc.Checklist(id='translate-check-box',options=[{'label':'Translate','value':"TF"}], value=['TF']), style={'display':'inline-block','padding':'10px'}
                        ),
                        html.Div(
                            html.Button("Export Data", id='export-data-button', n_clicks=0), style={'display':'inline-block'}
                        ),
                        dcc.Input(debounce=True, id='export_filename', type='text', placeholder="Enter a file name", style={'display':'inline-block'} )      
                    ], style={
                        'textAlign':'center'
                    }),
                    html.Div([
                        html.Div(
                            dcc.Input(id='year-filter', type='text', placeholder="Filter by Year"), style={'display':'inline-block'}
                        ),
                        html.Div(
                            dcc.Input(id='project-no-filter', type='text', placeholder="Filter by Project Number"), style={'display':'inline-block'}
                        )
                    ]),
                dcc.Loading(
                    id='loading-box-0',
                    children=[
                    html.Div(id='outputDF-display-table')
                ])
            ]),
        ]),
        dcc.Tab(label='Accents', value='tabs-2', children=[
            # add elements here to edit accent tab
            html.Div([
                html.Div( children=[
                    html.Div(
                        id='word-cloud-container', style={'borderWidth': '1px', 'borderStyle': 'dashed',
                        'borderRadius': '10px',}                      
                    ),
                    dcc.Input(debounce=True, id='stopwords-field'),
                    ],
                    style={'width':'49%','display':'inline-block'}
                ),
                html.Div( children=[
                    html.Div(
                        dash_table.DataTable(
                            id='accent-dict-table',
                            data=pipeline.helvetasDataObject.accentDict.to_dict('records'),
                            columns=[{"name":"Accent {}".format(i + 1), "id":"Accent {}".format(i + 1), "selectable":True} for i in range(10)],
                            column_selectable="multi",
                            selected_columns=["Accent 1"]
                        )
                    ),
                    html.Div(
                        dcc.Graph(id='accent-pie-chart', style={'borderWidth': '1px', 'borderStyle': 'dashed',
                        'borderRadius': '10px',})
                    )],
                    style={'width':'40%','display':'inline-block','verticalAlign':'top','padding':'10px'}
                )
            ])
        ]),
        dcc.Tab(label='Working Fields', value='tabs-3', children=[
            # add elements here to edit working field tab
            html.Div([
                html.Div(children=[
                    dcc.Graph(id='outcome-pie-chart'),
                    dcc.Dropdown(id='wf-dropdown',
                                options=[{'label':year,'value':year} for year in pipeline.yearList],
                                value= pipeline.yearList[0] if pipeline.yearList != [] else '0')
                ], style={'width':'49%','display':'inline-block'}),
                html.Div(
                    dcc.Graph(id='confidence-histogram'),
                    style={'width':'49%','display':'inline-block'}
                )
            ])
        ])
    ]),
    #
    # Adding a hidden element to allow for a callback with no
    # output or input
    #
    html.Div(id='hidden-div0', style={'display':'none'}),
    html.Div(id='hidden-div1', style={'display':'none'}),
    html.Div(id='hidden-div2', style={'display':'none'})
])

def parse_contents(filename, contents):
    content_type, content_string = contents.split(',')
    decoded = base64.b64decode(content_string)
    df = None
    if 'xls' in filename:
        df = pd.read_excel(io.BytesIO(decoded))
    if "Synopsis" in filename:
        pipeline.synopsisDF = df
    elif "Preprocessed" in filename:
        pipeline.preprocessedDF = df
    else:
        pipeline.yearlyProgressDFList.append(df)
    
fileList = []
@app.callback(Output('filelist-string', 'children'),
              Input('datafile-upload', 'filename'),
              Input('datafile-upload', 'contents'))
def update_output(filename, contents):
    if filename is None:
        return "Upload a file!"
    else:
        fileListOut = [html.P("Files Uploaded: ")]
        for i in range(len(filename)):
            fileList.append(filename[i])
            parse_contents(filename[i], contents[i])
        for file in fileList:
            fileListOut.append(html.P("\n"))
            fileListOut.append(html.P(file))
        return fileListOut


def loadData():
    if pipeline.isDataLoaded:
        return
    if pipeline.synopsisDF is not None and pipeline.yearlyProgressDFList is not []:
        pipeline.loadRawData()
    elif pipeline.preprocessedDF is not None:
        print("Loading pre processed data")
        pipeline.loadRawData()
        # pipeline.translateDataFrames()
        return True

lastClicks = 0
@app.callback(Output('hidden-div0', 'children'),
              Input('load-data-button', 'n_clicks'),
              Input('translate-check-box','value'))
def loadButtonCallback(n_clicks, translate):
    if (n_clicks - lastClicks) > 0:
        loadData()
    if translate[0]:
        if pipeline.isDataLoaded:
            pipeline.translateDataFrames()
    if pipeline.isDataLoaded:
        # if not pipeline.dataIsLemmatized:
        pipeline.lemmatizeDataFrames()
        pipeline.findAccents()
        pipeline.findWFs()
        pipeline.generateYearList()
    return

exportClicks = 0
@app.callback(Output('hidden-div2', 'children'),
            Input('export-data-button', 'n_clicks'),
            Input('export_filename','value'))
def exportButtonCallback(n_clicks, value):
    if value is None or value == "":
        return
    if ".xlsx" not in value:
        return
    if (n_clicks - exportClicks) > 0:
        loadData()
    if pipeline.isDataLoaded:
        pipeline.helvetasDataObject.toFile(value)
    return

@app.callback(Output('outputDF-display-table', 'children'),
              Input('hidden-div0','children'),
              Input('hidden-div1','children'),
              Input('year-filter','value'),
              Input('project-no-filter','value'))
def updateTable(children, children2, yearFilter, projectNoFilter):
    print("attempteding to update table")    
    if not pipeline.isDataLoaded:
        print("can't update table, is the data laoded?")
        return
    else: 
        print("updating table")
        print(yearFilter)
        if ((yearFilter is not None) and (yearFilter != "")) and ((projectNoFilter is not None) and (projectNoFilter != "")):
            print("filtering by both")
            print(yearFilter is not None)
            print(yearFilter != "")
            tableData = pipeline.helvetasDataObject.accessRowByProjectNoAndYear(projectNoFilter, yearFilter)
        elif ((yearFilter is not None) and (yearFilter != "")):
            tableData = pipeline.helvetasDataObject.accessRowByYear(yearFilter)
        elif ((projectNoFilter is not None) and (projectNoFilter != "")):
            tableData = pipeline.helvetasDataObject.accessRowByProjectNo(projectNoFilter)
        else:
            tableData = pipeline.helvetasDataObject.outputDataFrame
        return html.Div(dash_table.DataTable(
                        id='outputDF-table',
                        data=tableData.to_dict('records'), 
                        columns=[{"name": i, "id": i} for i in pipeline.helvetasDataObject.outputDataFrame.columns],
                        # style_data={'white-space':'normal'},
                        style_cell={
                            'overflow': 'hidden',
                            'textOverflow': 'ellipsis',
                            'maxWidth':'180px',
                        },
                        style_table={'overflowX': 'auto'},
                        tooltip_data=[
                            {
                                column: {'value': str(value), 'type': 'markdown'}
                                for column, value in row.items()
                            } for row in tableData.to_dict('records')
                        ],
                        tooltip_duration=None
                    ))

@app.callback(Output('outcome-pie-chart','figure'),
              Input('hidden-div0','children'),
              Input('wf-dropdown','value'))
def updatePieChart(children,value):
    if pipeline.isDataLoaded:
        data = None
        if (value == '0' or value is None):
            data = pipeline.helvetasDataObject.outputDataFrame
        else:
            data = pipeline.helvetasDataObject.accessRowByYear(value)
        # data = pipeline.helvetasDataObject.outputDataFrame
        df = pd.concat([data["WF Outcome 1"],data["WF Outcome 2"],data["WF Outcome 4"],data["WF Outcome 5"]], ignore_index=True)
        # print(df)
        print("Updating pie chart...")
        values = df.value_counts().values
        labels = df.value_counts().index
        # print(values)
        fig = px.pie(names=labels, labels=labels, values=values)
        return fig

@app.callback(Output('word-cloud-container','children'),
              Input('hidden-div0','children'),
              Input('hidden-div1','children'),
              Input('accent-dict-table','selected_columns'))
def updateWordCloud(children,children2,value):
    print("wordcloud update")
    print(value)
    if value is None or value == []:
        return
    if pipeline.isDataLoaded:
        df = pd.concat([pipeline.helvetasDataObject.accentDict[col] for col in value])
        print(df)
        dfw = pd.concat([pipeline.helvetasDataObject.weightDict[col] for col in value])
        words = [word for word in df.values.tolist()]
        preweights = [word for word in dfw.values.tolist()]
             
        weights = []
        for weight in preweights:
            weights.append(max(200 * (weight / sum(preweights)), 20))
        # print(words)
        # print(weights)
        print("Updating word cloud chart...")
        di = dict(zip(words, weights))
        wc = WordCloud(width=1024, height=512  ,background_color="white").generate_from_frequencies(frequencies=di)
        wc.recolor(color_func=accent_color_func)
        wc_img = wc.to_image()
        with BytesIO() as buffer:
            wc_img.save(buffer, 'png')
            img2 = base64.b64encode(buffer.getvalue()).decode()
        return html.Img(src="data:image/png;base64," + img2, style={'width':'100%'})
    return
def accent_color_func(word, font_size, position, orientation, random_state=None,
                    **kwargs):
    colors = ["rgb(104, 14,5)",
              "rgb(163, 29,35)",
              "rgb(200, 101,22)",
              "rgb(221, 163,54)",
              "rgb(227, 206,156)",
              "rgb(2, 51,81)",
              "rgb(0, 83,128)",
              "rgb(69, 140,176)",
              "rgb(130, 179, 196)",
              "rgb(193, 198, 202)"]
    for i in range(10):
        df = pipeline.helvetasDataObject.accentDict["Accent {}".format(i + 1)]
        words = [val for val in df.values.tolist()]
        if word in words:
            return colors[i]
    return "rgb(0,0,0)"

@app.callback(Output('accent-dict-table','data'),
            Input('hidden-div0','children'),
            Input('hidden-div1','children'))
def updateAccentDict(children,children2):
    print("updating dict")
    if pipeline.isDataLoaded:
        return pipeline.helvetasDataObject.accentDict.to_dict('records')

@app.callback(Output('wf-dropdown','options'),
             Input('hidden-div0','children'),
             Input('hidden-div1','children'))
def updateDropdownOptions(children, children2):
    return [{'label':year,'value':year} for year in pipeline.yearList]

@app.callback(Output('accent-pie-chart','figure'),
              Input('hidden-div0','children'),
              Input('hidden-div1','children'))
def updateAccentPieChart(children,children2):
    if pipeline.isDataLoaded:
        df = pipeline.helvetasDataObject.outputDataFrame[["Accent {}".format(i + 1) for i in range(10)]]
        maxes = df.idxmax(axis=1)
        maxes = maxes.value_counts()
        maxes = maxes.sort_index()
        print("sorted maxes")
        print(maxes)        
        values = maxes.values
        labels = maxes.index
        print("labels:")
        print(labels)
        # print(values)
        fig = px.pie(names=labels, labels=labels, values=values)
        return fig

@app.callback(Output('hidden-div1','children'),
            Input('stopwords-field','value'))
def addStopWords(value):
    if value is None:
        return
    value = "".join(value)
    pipeline.standardizer.add_stopwords([value])
    if pipeline.isDataLoaded:
        pipeline.lemmatizeDataFrames()
        pipeline.findAccents()
        pipeline.findWFs()

@app.callback(Output('confidence-histogram','figure'),
                Input('hidden-div0','children'))
def updateCumSum(child):
    if pipeline.isDataLoaded:
        probas = pipeline.supModel.get_proba(pipeline.helvetasDataObject.outputDataFrame[["WF Outcome 1","WF Outcome 2","WF Outcome 4","WF Outcome 5"]])
        print("probas")
        print(probas)
        figure = px.histogram(probas, nbins=20)
        return figure

if __name__ == '__main__':
    app.run_server(debug=True)
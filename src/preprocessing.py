# Imports necessary modules
import nltk
import spacy
from gensim.models.phrases import Phraser, Phrases
from gensim.utils import simple_preprocess
nltk.download('stopwords')
from nltk.corpus import stopwords
from typing import Iterable

# Imports generic stopwords


class Standardizer:
    def __init__(self):
        self.nlp = spacy.load('en_core_web_sm', disable=['parser', 'ner'])
        self.stop_words = stopwords.words('english')
        self.stop_words.extend(['from', 'subject', 're', 'edu', 'use']) 
        # possible extra stopwords 'project', 'projects', 'support', 'increase', 'improve'])
        # CAUTION: with extra stopwords above topic coherence signifcatnly deceases
        
    def add_stopwords(self, add_stop_words: Iterable[str]):
        self.stop_words.extend(add_stop_words)
        
    def strings2words(self, strings):
        # FIXME Make use of generator later. Returned empty list generator was used. Unknown reason.
        return [simple_preprocess(string, deacc=True) for string in strings] 
        
    def remove_stopwords(self, tokens_with_stopwords: Iterable[Iterable[str]]): 
        print("stopwords:")
        print(self.stop_words)  
        retList = []
        for tokenList in tokens_with_stopwords:
            retList.append(
                [token for token in simple_preprocess(str(tokenList)) if token not in self.stop_words])
        return retList
        
    def lemmatization(self, texts, nlp, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
        """https://spacy.io/api/annotation"""
        texts_out = []
        for sent in texts:
            doc = nlp(" ".join(sent))
            texts_out.append(
                [token.lemma_ for token in doc if token.pos_ in allowed_postags])
        return texts_out
        
    def standardize(self, raw_strings: Iterable[str]) -> Iterable[Iterable[str]]:
        """Standardize an Iterable of strings.
        Remove stop words, punctuation, new line characters, and lemmatize each word.
    
        Args:
            text (Iterable[str]): Strings to process.
            nlp: Spacy NLP object.
    
        Returns:
            Iterable[Iterable[str]]: Processed list of tokens for each input string.
        """
    
        print("Standardizing...")
    
        # convert each strings to an iterable of contained words
        doc_words = self.strings2words(raw_strings)
    
        # Build the bigram and trigram models
        # higher threshold fewer phrases.
        bigram = Phrases(doc_words, min_count=5, threshold=100)
        # Faster way to get a sentence clubbed as a trigram/bigram
        bigram_mod = Phraser(bigram)
    
        # Remove Stop Words
        # print("doc words:")
        # print(doc_words)
        nonstop_doc_words = self.remove_stopwords(doc_words)
        # print("removed stop words")
        # print(nonstop_doc_words)
    
        # Form Bigramss
        data_words_bigrams = [bigram_mod[tokens] for tokens in nonstop_doc_words]
    
        # Do lemmatization keeping only noun, adj, vb, adv
        data_lemmatized = self.lemmatization(data_words_bigrams, self.nlp)
        return data_lemmatized
    
    
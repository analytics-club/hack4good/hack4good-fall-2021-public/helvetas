import sys

from numpy.lib.function_base import vectorize

from run_LDA import run_LDA
sys.path.append("src/dataloader")
sys.path.append("src/translator")
sys.path.append("src")
from preprocessing import Standardizer
from dataloader import HelvetasData
from supervised_model import SUPmodel
import gensim.corpora as corpora
import numpy as np
import translator as ts
import argparse
import json
import pandas as pd 
import models
import spacy

#TODO REMOVE AFTER DEBUG
from rich.traceback import install as rich_tb_install
rich_tb_install()

# yearlyProgressFileList = ["helvetas_data/Export_IP_Reporting_ASAnalysis_ProgressSheet 2016.xlsx"]
#                             # "helvetas_data/Export_IP_Reporting_ASAnalysis_ProgressSheet 2017.xlsx",
#                             # "helvetas_data/Export_IP_Reporting_ASAnalysis_ProgressSheet 2018.xlsx",
#                             # "helvetas_data/Export_IP_Reporting_ASAnalysis_ProgressSheet 2019.xlsx",
#                             # "helvetas_data/Export_IP_Reporting_ASAnalysis_ProgressSheet 2020.xlsx"]

# parser = argparse.ArgumentParser(description = 
# """Helvetas Natural Language Processing Project
# --lemmatizedDataFile: Specify an already lemmatized data file to be used, will skip the translation and lemmatization step. Usage: main.py --lemmatizedDataFile fileName sheetName
# --synopsisFileName: Specify the name of the synopsis file. Usage: main.py --synopsisFileName fileName
# --sampleOutputDataFile: Specify the file containing sample outputs, used to guide formatting. Usage: main.py --sampleOutputDataFile fileName
# --yearlyProgressFileList: Specify a list of year files to process.
# --translate: specifies whether or not to translate data to english.
# --lemmatizedDataFile: specifies a prelemmatized data file. Usage: --lemmatizedDataFile fileName sheetName (sheetname is optional)
# """)
# parser.add_argument('--synopsisFileName', default="helvetas_data/Export_IP_Reporting_ASAnalysis_Synopsis.xlsx")
# parser.add_argument('--yearlyProgressFileList', nargs='*', default = yearlyProgressFileList)
# parser.add_argument('--sampleOutputDataFile', default="helvetas_data/Hack4Good Helvetas Output(1).xlsx")
# parser.add_argument('--translate', action='store_true')
# parser.add_argument('--lemmatizedDataFile', nargs='*', default='')
# parser.add_argument('--outputFileName', default="outputDataHelvetas.xlsx")
# args = parser.parse_args()

class HelvetasPipeline:
    def __init__(self):
        # load spacey english
        self.nlp_en = spacy.load("en_core_web_lg")

        # load WF names
        self.class_labels_df = pd.read_csv("helvetas_data/helvetas_WF_names.csv")

        # class labels
        self.class_labels_en = list(self.class_labels_df["WF Names EN"])

        # SUPmodel
        self.supModel = SUPmodel()

        #standardizer
        self.standardizer = Standardizer()        

        # excel file names
        self.synopsisFileName = "" # this is a string
        self.yearlyProgressFileList = [] # This is of type list!
        self.sampleOutputDataFile = "" # this is a string 

        # helvetas data object
        self.helvetasDataObject = HelvetasData(None, None, None) # of type Helvetas Data Object

        # dataframes (to skip loading from file names)
        self.synopsisDF = None # this is a string
        self.yearlyProgressDFList = [] # This is of type list!
        self.preprocessedDF = None

        # switch variables for keeping track of data
        self.isDataLoaded = False
        self.dataIsTranslated = False
        self.dataIsLemmatized = False
        self.outputFileName = ""

        # find the list of years that are available
        self.yearList = []

    # Load excel data 
    # (not done by default, because excel file names may or may not be empty)
    def loadRawData(self):
        print("Loading Helvetas data from files")
        self.sampleOutputDataFile = "helvetas_data/Hack4Good Helvetas Output(1).xlsx"
        if self.preprocessedDF is not None:
            print("Loading into object from preprocessed file")
            self.helvetasDataObject.outputDataFrame = self.preprocessedDF.copy(deep=True)
            self.isDataLoaded = True
            self.dataIsTranslated = True
        else:
            print("Loading data from files")
            self.helvetasDataObject = HelvetasData(self.synopsisDF, self.yearlyProgressDFList, self.sampleOutputDataFile)
            self.isDataLoaded = True

    # Translate data frames
    def translateDataFrames(self):
        if not self.dataIsTranslated:
            print("Translating data...")
            vtranslate=np.vectorize(ts.translate)
            translatedDF = self.helvetasDataObject.rawDataFrame.copy(deep=True)
            print("Translating data into English, this may take a while...")
            translatedDF["Outcome 1"] = vtranslate(translatedDF["Outcome 1"])
            print("Translating outcome 1")
            translatedDF["Description"] = vtranslate(translatedDF["Description"])
            print("Translating outcome 2")
            translatedDF["Outcome 2"] = vtranslate(translatedDF["Outcome 2"])
            print("Translating outcome 3")
            translatedDF["Outcome 3"] = vtranslate(translatedDF["Outcome 3"])
            print("Translating outcome 4")
            translatedDF["Outcome 4"] = vtranslate(translatedDF["Outcome 4"])
            print("Translating outcome 5")
            translatedDF["Outcome 5"] = vtranslate(translatedDF["Outcome 5"])
            self.helvetasDataObject.setOutputDataFrame(translatedDF)
            self.dataIsTranslated = True


    # Lemmatize data frames
    def lemmatizeDataFrames(self):
        print("Lemmatizing the data... this could take a while...")
        lemmaDF = pd.DataFrame()
        if self.dataIsTranslated:
            lemmaDF = self.helvetasDataObject.outputDataFrame.copy(deep=True)
        else:
            lemmaDF = self.helvetasDataObject.rawDataFrame.copy(deep=True)
        text = lemmaDF["Description"].astype(str)
        print("type: ")
        print(text[0])
        lemmaDF["Description"] = self.standardizer.standardize(lemmaDF["Description"].astype(str).tolist())
        lemmaDF["Outcome 1"] = self.standardizer.standardize(lemmaDF["Outcome 1"].astype(str).tolist())
        lemmaDF["Outcome 2"] = self.standardizer.standardize(lemmaDF["Outcome 2"].astype(str).tolist())
        lemmaDF["Outcome 3"] = self.standardizer.standardize(lemmaDF["Outcome 3"].astype(str).tolist())
        lemmaDF["Outcome 4"] = self.standardizer.standardize(lemmaDF["Outcome 4"].astype(str).tolist())
        lemmaDF["Outcome 5"] = self.standardizer.standardize(lemmaDF["Outcome 5"].astype(str).tolist())
        self.helvetasDataObject.setLemmatizedDataFrame(lemmaDF)
        print("Finished Lemmatizing!")
        self.dataIsLemmatized = True


    # Create Dictionary of ALL project descriptions
    def findAccents(self):
        lemmData = self.helvetasDataObject.lemmatizedDataFrame
        data = lemmData["Description"]
        id2word = corpora.Dictionary(data)

        # Create Corpus
        print("Building corpus...")
        texts = lemmData["Description"].array
        # Term Document Frequency
        corpus = [id2word.doc2bow(text) for text in data]
        print("Running LDA model to generate and assign accents")
        wordList, result = run_LDA(id2word, corpus)
        # Place into all dataframes
        self.helvetasDataObject.outputDataFrame["Accent 1"] = result["Topic 0"]
        self.helvetasDataObject.outputDataFrame["Accent 2"] = result["Topic 1"]
        self.helvetasDataObject.outputDataFrame["Accent 3"] = result["Topic 2"]
        self.helvetasDataObject.outputDataFrame["Accent 4"] = result["Topic 3"]
        self.helvetasDataObject.outputDataFrame["Accent 5"] = result["Topic 4"]
        self.helvetasDataObject.outputDataFrame["Accent 6"] = result["Topic 5"]
        self.helvetasDataObject.outputDataFrame["Accent 7"] = result["Topic 6"]
        self.helvetasDataObject.outputDataFrame["Accent 8"] = result["Topic 7"]
        self.helvetasDataObject.outputDataFrame["Accent 9"] = result["Topic 8"]
        self.helvetasDataObject.outputDataFrame["Accent 10"] = result["Topic 9"]



        # build topic dictionary
        dictDF = pd.DataFrame(columns=['Accent 1', 'Accent 2','Accent 3','Accent 4','Accent 5','Accent 6','Accent 7','Accent 8','Accent 9', 'Accent 10',])
        print("accent dict: ")
        dictDF["Accent 1"] = [word[0] for word in wordList[0]]
        dictDF["Accent 2"] = [word[0] for word in wordList[1]]
        dictDF["Accent 3"] = [word[0] for word in wordList[2]]
        dictDF["Accent 4"] = [word[0] for word in wordList[3]]
        dictDF["Accent 5"] = [word[0] for word in wordList[4]]
        dictDF["Accent 6"] = [word[0] for word in wordList[5]]
        dictDF["Accent 7"] = [word[0] for word in wordList[6]]
        dictDF["Accent 8"] = [word[0] for word in wordList[7]]
        dictDF["Accent 9"] = [word[0] for word in wordList[8]]
        dictDF["Accent 10"] = [word[0] for word in wordList[9]]
        print(dictDF)
        # build topic dictionary
        weightDF = pd.DataFrame(columns=['Accent 1', 'Accent 2','Accent 3','Accent 4','Accent 5','Accent 6','Accent 7','Accent 8','Accent 9', 'Accent 10',])
        print("weight dict")
        weightDF["Accent 1"] = [word[1] for word in wordList[0]]
        weightDF["Accent 2"] = [word[1] for word in wordList[1]]
        weightDF["Accent 3"] = [word[1] for word in wordList[2]]
        weightDF["Accent 4"] = [word[1] for word in wordList[3]]
        weightDF["Accent 5"] = [word[1] for word in wordList[4]]
        weightDF["Accent 6"] = [word[1] for word in wordList[5]]
        weightDF["Accent 7"] = [word[1] for word in wordList[6]]
        weightDF["Accent 8"] = [word[1] for word in wordList[7]]
        weightDF["Accent 9"] = [word[1] for word in wordList[8]]
        weightDF["Accent 10"] = [word[1] for word in wordList[9]]
        print(weightDF)

        # print(dictDF)
        self.helvetasDataObject.accentDict = dictDF
        self.helvetasDataObject.weightDict = weightDF

    def findWFs(self):
        # # create model WF classification model
        # print("Building embedding classifier model...")
        # EC_en_euclid = models.EmbeddingClassifier(self.class_labels_en, self.nlp_en)

        # # predict WF
        # print("Generating predictions....")
        # wfOutcome1Prediction = EC_en_euclid.predict(self.helvetasDataObject.lemmatizedDataFrame["Outcome 1"])
        # wfOutcome2Prediction = EC_en_euclid.predict(self.helvetasDataObject.lemmatizedDataFrame["Outcome 2"])
        # wfOutcome4Prediction = EC_en_euclid.predict(self.helvetasDataObject.lemmatizedDataFrame["Outcome 4"])
        # wfOutcome5Prediction = EC_en_euclid.predict(self.helvetasDataObject.lemmatizedDataFrame["Outcome 5"])
        # # add formatted data
        # self.helvetasDataObject.outputDataFrame["WF Outcome 1"] = wfOutcome1Prediction
        # self.helvetasDataObject.outputDataFrame["WF Outcome 2"] = wfOutcome2Prediction
        # self.helvetasDataObject.outputDataFrame["WF Outcome 3"] = wfOutcome4Prediction
        # self.helvetasDataObject.outputDataFrame["WF Outcome 4"] = wfOutcome5Prediction

        # Using supervised learning model:
        self.supModel.fit(self.helvetasDataObject.lemmatizedDataFrame["Description"].copy(deep=True), self.helvetasDataObject.outputDataFrame["Project WF"].copy(deep=True))
        wfOutcome1Prediction = self.supModel.predict(self.helvetasDataObject.lemmatizedDataFrame["Outcome 1"])
        wfOutcome2Prediction = self.supModel.predict(self.helvetasDataObject.lemmatizedDataFrame["Outcome 2"])
        wfOutcome3Prediction = self.supModel.predict(self.helvetasDataObject.lemmatizedDataFrame["Outcome 3"])
        wfOutcome4Prediction = self.supModel.predict(self.helvetasDataObject.lemmatizedDataFrame["Outcome 4"])
        wfOutcome5Prediction = self.supModel.predict(self.helvetasDataObject.lemmatizedDataFrame["Outcome 5"])
        # add formatted data
        self.helvetasDataObject.outputDataFrame["WF Outcome 1"] = wfOutcome1Prediction
        self.helvetasDataObject.outputDataFrame["WF Outcome 2"] = wfOutcome2Prediction
        self.helvetasDataObject.outputDataFrame["WF Outcome 3"] = wfOutcome3Prediction
        self.helvetasDataObject.outputDataFrame["WF Outcome 4"] = wfOutcome4Prediction
        self.helvetasDataObject.outputDataFrame["WF Outcome 5"] = wfOutcome5Prediction

    def generateYearList(self):
        print("generating year list")
        df = self.helvetasDataObject.outputDataFrame["Year"]
        yl = df.values.tolist()
        yl = set(yl)
        self.yearList = yl

    def exportData(self):
        # save to file
        print("Saving to file...")
        self.helvetasDataObject.toFile(self.outputFileName)





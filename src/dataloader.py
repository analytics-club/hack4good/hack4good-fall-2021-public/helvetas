import pandas as pd 

class HelvetasData:

    # def __init__(self):
    #     self.outputDataFrame = pd.DataFrame()
    #     self.lemmatizedDataFrame = pd.DataFrame()
    #     self.rawDataFrame = pd.DataFrame()
    #     self.accentDict = pd.DataFrame()
    #     self.yearlyProgressDFList = []
    #     self.synopsisDF = pd.DataFrame()
    #     self.sampleOutputDataFile = ""
    #     self.synopsisDataFrame = pd.DataFrame()
    
    def __init__(self, synopsisDF, yearlyProgressDFList, sampleOutputDataFile):
        self.outputDataFrame = pd.DataFrame()
        self.lemmatizedDataFrame = pd.DataFrame()
        self.rawDataFrame = pd.DataFrame()
        self.accentDict = pd.DataFrame()
        self.weightDict = pd.DataFrame()
        self.yearlyProgressDFList = yearlyProgressDFList
        self.synopsisDF = synopsisDF
        self.sampleOutputDataFile = sampleOutputDataFile
        self.synopsisDataFrame = synopsisDF
        self.yearList = []
        if synopsisDF is not None:
            self.generateDataFrame()

    def setRawDataFrame(self, df):
        self.rawDataFrame = df

    def setLemmatizedDataFrame(self, df):
        self.lemmatizedDataFrame = df

    def setOutputDataFrame(self, df):
        self.outputDataFrame = df

    # Arguments:
    # synopsisFileName: the name of the synopsis file (duh)
    # yearlyProgressFileList: list of years of files to by used
    # sampleOutputDataFile: format to use for output file
    def generateDataFrame(self):
        # import the synopsis data (raw data source) and the sample output to get use as a reference for the structure of the output dataframe
        # Copy and paste the contents of the synopsis data into the fields of the output data frame.
        # Using the following rule, as a guide (found in Hack4Good Helvetas Output.xlsx)
        # Year - column N (yearly progress sheet file)
        # Project # - column G
        # Project Name - column I
        # Country - column F
        # Description - column S
        # Development Goal - column T
        # Project WA - column L
        # Project WF - column M
        # Outcome 1 - column S (yearly progress sheet)
        # KPI 1 - column X
        # Outcome 2 - column V (yearly progress sheet)
        # KPI 2 - column Z
        # Outcome 3 - Column AA (yearly progress sheet)
        # KPI 3 - column AB
        # Outcome 4 - column AC
        # KPI 4 - AD
        # Outcome 5 - column AD
        # KPI - column AF
        # The output file should have the same number of rows as the combined total of ALL yearly data
        sampleOutputDataFrame = pd.read_excel(self.sampleOutputDataFile)
        pdfs = []
        for yearlyProgressDF in self.yearlyProgressDFList:
            yearlyOutputDataFrame = pd.DataFrame(columns=sampleOutputDataFrame.columns)
            yearlyOutputDataFrame["Year"] = yearlyProgressDF["Year"] # copy over the year column
            yearlyOutputDataFrame["Project #"] = yearlyProgressDF["Project_No"] # copy over the year column
            yearlyOutputDataFrame["Project Name"] = yearlyProgressDF["Project_Name"]
            yearlyOutputDataFrame["Country"] = yearlyProgressDF["Country_Name"]
            yearlyOutputDataFrame["Project WA"] = yearlyProgressDF["WorkingArea"]
            yearlyOutputDataFrame["Project WF"] = yearlyProgressDF["WorkingField"]
            yearlyOutputDataFrame["Outcome 1"] = yearlyProgressDF["Outcome1Realised"]
            yearlyOutputDataFrame["Outcome 2"] = yearlyProgressDF["Outcome2Realised"]
            yearlyOutputDataFrame["Outcome 3"] = yearlyProgressDF["Outcome3Realised"] # outcome 2 is currently broken :(
            yearlyOutputDataFrame["Outcome 4"] = yearlyProgressDF["Outcome4Realised"]
            yearlyOutputDataFrame["Outcome 5"] = yearlyProgressDF["Outcome5Realised"]
            count = 0
            for (idx, row) in yearlyOutputDataFrame.iterrows():
                
                row["Description"] = self.synopsisDataFrame[self.synopsisDataFrame["Project_No"] == row["Project #"]]["Short_description"]
                tmpStr = str(row["Description"].values)

                yearlyOutputDataFrame.loc[idx, "Description"]= tmpStr
            pdfs.append(yearlyOutputDataFrame)
        self.rawDataFrame = pd.concat(pdfs) 
        self.outputDataFrame = self.rawDataFrame.copy(deep=True)

    # write the dataframe to a file
    def toFile(self, fileName):
        # lemmatizedFileName = "lemmatized_" + fileName
        writer = pd.ExcelWriter(fileName, engine='xlsxwriter')
        self.outputDataFrame.to_excel(writer,sheet_name='Data Sheet')
        self.rawDataFrame.to_excel(writer, sheet_name='Raw Data Sheet')
        self.lemmatizedDataFrame.to_excel(writer,sheet_name='Lemmatized Data Sheet')      
        self.accentDict.to_excel(writer, sheet_name='Accent Dictionary')  
        writer.save()

        writer_preproc = pd.ExcelWriter("Preprocessed_Output.xlsx", engine='xlsxwriter')
        self.outputDataFrame.to_excel(writer_preproc,sheet_name='Data Sheet')
        writer_preproc.save()

    def accessRowByProjectNo(self, project_no):
        if type(project_no) is str:
            project_no = int(project_no)
        return self.outputDataFrame.loc[self.outputDataFrame["Project #"] == project_no]

    def accessRowByYear(self, year):
        # if not is numeric(year)   
        if type(year) is str:
            year = int(year)
        return self.outputDataFrame.loc[self.outputDataFrame["Year"] == year]

    def accessRowByProjectNoAndYear(self, project_no, year):
        if type(project_no) is str:
            project_no = int(project_no)
        if type(year) is str:
            year = int(year)
        return self.outputDataFrame.loc[(self.outputDataFrame["Project #"] == project_no) & (self.outputDataFrame["Year"] == year)]

    def loadLemmatizedData(self, fileName, sheetName):
        if sheetName == '':
            self.lemmatizedDataFrame = pd.read_excel(fileName)
        else:
            self.lemmatizedDataFrame = pd.read_excel(fileName, sheet_name = sheetName)









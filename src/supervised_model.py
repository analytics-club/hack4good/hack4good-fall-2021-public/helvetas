from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.calibration import CalibratedClassifierCV
from sklearn.svm import LinearSVC
from typing import Iterable


class SUPmodel:
    def __init__(self):
        self.vectorizer = TfidfVectorizer()
        self.svc = LinearSVC()
        self.clf = CalibratedClassifierCV(self.svc)
        
    def clean_data(self, descriptions: Iterable[Iterable[str]], working_fields: Iterable[str]):
        for i in range(len(working_fields)):
            if working_fields.isnull().values[i]:
                del descriptions[i]
        working_fields.dropna(inplace=True)
        return descriptions, working_fields
        
    def convert_to_string(self, list_: Iterable[str]) -> str :
        single_string = ""
        for str_ in list_:
            single_string += ' ' + str_
        return single_string

    def convert_to_list(self, descriptions: Iterable[Iterable[str]]) -> Iterable[str]:
        list_ = []
        for element in descriptions:
            list_.append(self.convert_to_string(element))
        return(list_)

    def fit(self, descriptions: Iterable[Iterable[str]], working_fields: Iterable[str]):
        """Trains a Tfidf-vectorizer and a CalibratedClassifierCV 
        with a linear-svc as a base estimator, robust to NaNs"""
        descriptions, working_fields = self.clean_data(descriptions, working_fields)
        X_train = self.vectorizer.fit_transform(self.convert_to_list(descriptions))
        self.clf.fit(X_train, working_fields)
        
    def predict(self, outcomes: Iterable[Iterable[str]]) -> Iterable[str]:
        vectorized_outcomes = self.vectorizer.transform(self.convert_to_list(outcomes))
        predicted_wfs = self.clf.predict(vectorized_outcomes)
        return predicted_wfs
    
    def get_proba(self, outcomes: Iterable[Iterable[str]]) -> Iterable[int]:
        vectorized_outcomes = self.vectorizer.transform(self.convert_to_list(outcomes))
        y_proba = self.clf.predict_proba(vectorized_outcomes)
        highest_proba_list = []
        for i, row in enumerate(y_proba):
            row = sorted(row, reverse=True)
            highest_proba_list.append(round(row[0], 3))
        return highest_proba_list


        
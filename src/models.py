"""Interfaces and implementations of models for WF Classification
"""
import preprocessing as pp

from collections import OrderedDict
from typing import (Callable, Dict, Iterable, Optional, OrderedDict, Tuple,
                    Union)

import numpy as np
import spacy
from sklearn.neighbors import NearestNeighbors


class WFClassifier:
    """General model interface. Models implemented here should have the
    following methods implemented, otherwise a NotImplementedException
    """

    def __init__(self, class_labels: Iterable[str]) -> None:
        self.class_labels = np.asarray(class_labels)
        self.num_classes = len(class_labels)

    def assign_confidence(self, input: Iterable[str]) -> Dict[str, float]:
        # TODO: Change output type to a more general Dict[int, float]
        #   and deal with idx -> class name map in predict().
        """Given the input of lemmatized words, return an ordered
        dictionary of class index and a corresponding confidence level
        (larger means more confident).

        Args:
            input (Iterable[str]): Iterable of lemmatized words belonging to a
                document which is to be classified into self.classes

        Returns:
            Dict[str, float]: A dictionary of classes and confidences.
        """
        raise NotImplementedError(
            f"The class {self.__class__.__name__} does not have the method "
            + "assign_confidence() implemented.")

    def predict(self, inputs: Iterable[str], give_top=1, give_confidence=False) -> Union[Iterable[str], OrderedDict[str, float]]:
        """Given an input iterable of lemmatized words, return the
        class(es) with the highest confidence of classification, with or
        without said confidence. 

        Args:
            input (Iterable[str]): Iterable of lemmatized words belonging to a
                document which is to be classified into classes
            give_top (int, optional): The number of classes to return
                according to their confidence in decreasing order.
                TODO:If None,return all classes whose confidence exceeds self.threshold.
                Defaults to 1.
            give_confidence (bool optional): If True, return the class
                and its confidence as a tuple. Defaults to False.
        """
        if give_top is not None and give_top > len(self.class_labels):
            raise ValueError(
                f"The 'give_top' parameter cannot exceed the number of classes. "
                + f"Got {give_top}, can be at most {len(self.class_labels)}.")

        confidences = self.assign_confidence(inputs)

        top_classes = sorted(confidences.items(),
                             key=lambda x: x[1], reverse=True)[:give_top]
        top_classes = OrderedDict(top_classes)

        return top_classes if give_confidence else list(top_classes.keys())


class EmbeddingClassifier(WFClassifier):
    """An unsupervised classifier which uses measures the similarity of
    input embeddings with whose of the classes and assigns a confdance-
    level to each class.
    """

    def __init__(self, class_labels: Iterable[str], nlp, class_descriptors: Optional[Iterable[Iterable[str]]] = None, similarity_measure: Union[str, Callable] = 'minkowski') -> None:
        """
        Args:
            classes (Iterable[str]): Categorical classes.
            class_descriptors (Dict[str, Iterable[str]], optional): Lemmatized tokens heavily semantically associated with each class. Used to compute a more representative class embedding. Defaults to None.
            similarity_measure (Union[str, Callable], optional): A real function for computing the similarity of two embeddings. Defaults to 'minkowski'.
        """
        super().__init__(class_labels)
        self.class_descriptors = class_descriptors
        self.similarity_measure = similarity_measure
        self._nlp = nlp

        standardized_class_labels = pp.standardize(self.class_labels, self._nlp)

        if self.class_descriptors is None:
            class_embeddings_source = standardized_class_labels
        else:
            class_embeddings_source = [
                standardized_class_labels[i] + self.class_descriptors[i] for i in range(self.num_classes)]

        self._class_embeddings = [self._embed(tokens)
                                 for tokens in class_embeddings_source]

        self._neigh = NearestNeighbors(
            n_neighbors=self.num_classes, metric=self.similarity_measure)
        self._neigh.fit(self._class_embeddings)

    def _embed(self, tokens):
        lexs = (self._nlp.vocab[token] for token in tokens)
        embeddings = [lex.vector for lex in lexs if lex.has_vector]

        return np.mean(embeddings, axis=0) if len(embeddings) > 0 else np.zeros(self._nlp.meta['vectors']['width'])

    def _runNN(self, inputs, k=1, return_distances=False):
        """Run the internal Nearest Neighbors predictor on the inputs
        with given parameters.

        Args:
            inputs (Iterable[Iterable[str]]): Iterable of Standardized samples. 
            k (int, optional): [description]. Number of nearest neighbors to return Defaults to 1.
            return_distances (bool, optional): Wether to return an ndarray of corresponding distances. Defaults to False.
        """
        centroids = [self._embed(tokens) for tokens in inputs]

        return self._neigh.kneighbors(centroids, k, return_distances)


    def assign_confidence(self, inputs):
        """Return an ndarray of prediction labels and a corresponding array of distances of the samples to each class.

        Args:
            inputs (Iterable[Iterable[str]]): Iterable of Standardized samples. 

        Returns:
            pred_labels: Numpy array of class labels for each sample (closest to farthest)
            dists: Numpy array of class-corresponding distances for each sample
        """

        dists, pred = self._runNN(inputs, self.num_classes, return_distances=True)

        # get class labels
        pred_labels = self.class_labels[pred]

        return pred_labels, dists

    def predict(self, inputs: Iterable[Iterable[str]], give_top=1, give_confidence=False):
        nn_output = self._runNN(inputs, give_top, give_confidence)

        if give_confidence:
            dists, pred = nn_output

            # get class labels
            pred_labels = self.class_labels[pred]
            return pred_labels, dists
        else:
            pred = nn_output

            # get class labels
            pred_labels = self.class_labels[pred]
            return pred_labels

import gensim
from nltk import corpus
import pandas as pd

def format_topics_sentences(ldamodel=None, corpus=corpus, number_of_topics=None):
    # Initialize output
    sent_topics_df = pd.DataFrame()

    # Get and sort (topic_num, prop_topic) for each document
    for i, row_list in enumerate(ldamodel[corpus]):
        row = row_list[0] if ldamodel.per_word_topics else row_list            
        row = sorted(row, key=lambda x: (x[1]), reverse=True)

        # Get the Topic Distribution
        l = [0] * number_of_topics
        for j, (topic_num, prop_topic) in enumerate(row):
            l[topic_num] = prop_topic
        sent_topics_df = sent_topics_df.append(pd.Series(l), ignore_index=True)
            
    
    c = [None] * number_of_topics
    for i in range(number_of_topics): c[i] = f"Topic {i}" 
    sent_topics_df.columns = c
    sent_topics_df.index.name = 'Document_No' 
    
    return(sent_topics_df)

### This function expects id2word and corpus, default topic number is 10
### It returns a list of lists (the word-2-topic model) as its first argument
### It returns a dataframe (the topic-2-document model) as its second argument
def run_LDA(id2word=None, corpus=None, number_of_topics=10):
    
    # Build LDA model
    lda_model = gensim.models.ldamodel.LdaModel(corpus=corpus,
                                           id2word=id2word,
                                           num_topics = number_of_topics, 
                                           random_state=100,
                                           update_every=1,
                                           chunksize=100,
                                           passes=10,
                                           alpha='auto',
                                           per_word_topics=True)


    top_10_words = [0] * number_of_topics
    for i in range(10): top_10_words[i] = lda_model.show_topic(i)
    
    df_topic_distribution = format_topics_sentences(ldamodel=lda_model, corpus=corpus, number_of_topics=10)

    return top_10_words, df_topic_distribution.round(2)
    
    

from deep_translator import (GoogleTranslator)
from deep_translator import single_detection

def translate(text, target_lang="en"):
  if isinstance(text, str) and len(text) > 3:
    # print("%i: %s" % (len(text), text))
    text = text + " "
    if len(text) > 5000:
      # print("found a string of length %s" % text(len))
      parts = []
      its = floor(len(text) / 5000)
      for i in range (its):
        sbstr = text[i * 5000: (i * 5000) + 4999]
        parts.append(GoogleTranslator(source='auto', target=target_lang).translate(text=sbstr))
      parts.append(GoogleTranslator(source='auto', target=target_lang).translate(text=text[(its * 5000):end]))
      return " ".join(parts)
    else: 
      return GoogleTranslator(source='auto', target=target_lang).translate(text=text)
  else:
    return text

# def detectEnglish(text):
#   result = GoogleTranslator().single_detection(text)
#   if result.language == 'en':
#     return True
#   else:
#     return False



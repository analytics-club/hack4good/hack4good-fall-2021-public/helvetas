#! /usr/bin/env bash

RED='\033[1;31m'
GREEN='\033[1;32m'
CYAN='\033[1;36m'
NC='\033[0m' # No Color

# Activate the environemnt given as argument
if [ $# -eq 0 ]; then
    ENV_NAME="h4g"
    echo -e $CYAN"No environment name given. Using \"${ENV_NAME}\" as default."$NC
else
    ENV_NAME=${1}
fi

if ! (return 0 2>/dev/null) ; then
    # If return is used in the top-level scope of a non-sourced script,
    # an error message is emitted, and the exit code is set to 1

    echo
    echo -e $RED"Note: To activate the environment in the shell use"$NC
    echo
    echo "        conda activate ${ENV_NAME}"
    echo
    echo -e $RED"... or source this script via "$NC
    echo
    echo "        source ./scripts/activate.sh ${ENV_NAME}"
    echo
    exit 1  # we detected we are NOT source'd so we can use exit
fi

if type conda &>/dev/null; then
    if conda info --envs | grep ${ENV_NAME} &>/dev/null; then
        echo -e $CYAN"Activating environment \"${ENV_NAME}\"..."$NC
    else
      echo
      echo -e $RED"(!) Environemnt ${ENV_NAME} not found!"$NC
      echo -e $RED"(!) Please install the conda environment ${ENV_NAME}."$NC
      echo
      return 1  # we are source'd so we cannot use exit
    fi
else
    echo
    echo -e $RED"(!) Please install anaconda"$NC
    echo "conda could not be found, please install it by reffering to https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html"
    echo
    return 1  # we are source'd so we cannot use exit
fi

eval "$(conda shell.bash hook)" && conda activate ${ENV_NAME} && echo -e $GREEN"Environment \"${ENV_NAME}\" activated!"$NC && return 0

return 1
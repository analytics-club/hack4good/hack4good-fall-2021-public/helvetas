#! /usr/bin/env bash

RED='\033[1;31m'
GREEN='\033[1;32m'
CYAN='\033[1;36m'
NC='\033[0m' # No Color

ENV_NAME="h4g"

# Script to set up the environment for the Helvetas project.

# Python Environment set up

 if ! command -v python3 &>/dev/null; then
    echo -e $RED"python3 could not be found, please install it from https://www.python.org/downloads"$NC && exit 1
  fi

  echo "" && echo -e $CYAN"Using python version:"$NC;
  python3 -c "import sys; print(sys.version)";

  echo -e $CYAN"Setting up conda environment..."$NC
  if type conda &>/dev/null; then
      if conda info --envs | grep ${ENV_NAME} &>/dev/null; then
        echo -e $CYAN"Conda Environment ${ENV_NAME} found!"$NC
        echo -e $CYAN"Updating environment..."$NC
        conda env update --name ${ENV_NAME} --file env.yaml && echo -e $GREEN"Done!"$NC
      else
        echo
        echo -e $RED"(!) Environment ${ENV_NAME} not found!"$NC
        echo -e $CYAN"Installing via conda..."$NC
        echo
        conda env create --name ${ENV_NAME} -f env.yaml && echo -e $GREEN"Environment ${ENV_NAME} is installed!"$NC
        echo -e $CYAN"Installing Spacy English pipeline..."$NC
        python -m spacy download en_core_web_trf && echo -e $GREEN"Pipeline is installed!"$NC
      fi
  else
      echo
      echo -e $RED"(!) Please install anaconda"$NC
      echo "conda could not be found, please install it by reffering to https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html"
      echo
      exit 1
  fi

exit 0


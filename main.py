import sys

from numpy.lib.function_base import vectorize

# from run_LDA import run_LDA
# import supervised_model as spv
sys.path.append("src/dataloader")
sys.path.append("src/translator")
sys.path.append("src")
from dataloader import HelvetasData
from pipeline import HelvetasPipeline
from run_LDA import run_LDA
import gensim.corpora as corpora
import preprocessing as pp
import numpy as np
import translator as ts
import argparse
import json
import pandas as pd 
import models
import spacy

#TODO REMOVE AFTER DEBUG
from rich.traceback import install as rich_tb_install
rich_tb_install()

yearlyProgressFileList = ["helvetas_data/Export_IP_Reporting_ASAnalysis_ProgressSheet_2016.xlsx"]
                            # "helvetas_data/Export_IP_Reporting_ASAnalysis_ProgressSheet_2017.xlsx",
                            # "helvetas_data/Export_IP_Reporting_ASAnalysis_ProgressSheet_2018.xlsx",
                            # "helvetas_data/Export_IP_Reporting_ASAnalysis_ProgressSheet_2019.xlsx",
                            # "helvetas_data/Export_IP_Reporting_ASAnalysis_ProgressSheet_2020.xlsx"]

parser = argparse.ArgumentParser(description = 
"""Helvetas Natural Language Processing Project
--lemmatizedDataFile: Specify an already lemmatized data file to be used, will skip the translation and lemmatization step. Usage: main.py --lemmatizedDataFile fileName sheetName
--synopsisFileName: Specify the name of the synopsis file. Usage: main.py --synopsisFileName fileName
--sampleOutputDataFile: Specify the file containing sample outputs, used to guide formatting. Usage: main.py --sampleOutputDataFile fileName
--yearlyProgressFileList: Specify a list of year files to process.
--translate: specifies whether or not to translate data to english.
--lemmatizedDataFile: specifies a prelemmatized data file. Usage: --lemmatizedDataFile fileName sheetName (sheetname is optional)
""")
parser.add_argument('--synopsisFileName', default="helvetas_data/Export_IP_Reporting_ASAnalysis_Synopsis.xlsx")
parser.add_argument('--yearlyProgressFileList', nargs='*', default = yearlyProgressFileList)
parser.add_argument('--sampleOutputDataFile', default="helvetas_data/Hack4Good Helvetas Output(1).xlsx")
parser.add_argument('--translate', action='store_true')
parser.add_argument('--lemmatizedDataFile', nargs='*', default='')
parser.add_argument('--outputFileName', default="outputDataHelvetas.xlsx")
args = parser.parse_args()

pipeline = HelvetasPipeline()

pipeline.yearlyProgressFileList = args.yearlyProgressFileList
pipeline.outputFileName = args.outputFileName
pipeline.synopsisFileName = args.synopsisFileName
pipeline.sampleOutputDataFile = args.sampleOutputDataFile

pipeline.synopsisDF = pd.read_excel(pipeline.synopsisFileName)
pipeline.yearlyProgressDFList = []
for file in pipeline.yearlyProgressFileList:
    pipeline.yearlyProgressDFList.append(pd.read_excel(file))

pipeline.loadRawData()

pipeline.translateDataFrames()
pipeline.lemmatizeDataFrames()
pipeline.findAccents()
pipeline.findWFs()

pipeline.exportData()

# yearlyProgressFileList = ["helvetas_data/Export_IP_Reporting_ASAnalysis_ProgressSheet 2016.xlsx"]
#                             # "helvetas_data/Export_IP_Reporting_ASAnalysis_ProgressSheet 2017.xlsx",
#                             # "helvetas_data/Export_IP_Reporting_ASAnalysis_ProgressSheet 2018.xlsx",
#                             # "helvetas_data/Export_IP_Reporting_ASAnalysis_ProgressSheet 2019.xlsx",
#                             # "helvetas_data/Export_IP_Reporting_ASAnalysis_ProgressSheet 2020.xlsx"]

# parser = argparse.ArgumentParser(description = 
# """Helvetas Natural Language Processing Project
# --lemmatizedDataFile: Specify an already lemmatized data file to be used, will skip the translation and lemmatization step. Usage: main.py --lemmatizedDataFile fileName sheetName
# --synopsisFileName: Specify the name of the synopsis file. Usage: main.py --synopsisFileName fileName
# --sampleOutputDataFile: Specify the file containing sample outputs, used to guide formatting. Usage: main.py --sampleOutputDataFile fileName
# --yearlyProgressFileList: Specify a list of year files to process.
# --translate: specifies whether or not to translate data to english.
# --lemmatizedDataFile: specifies a prelemmatized data file. Usage: --lemmatizedDataFile fileName sheetName (sheetname is optional)
# """)
# parser.add_argument('--synopsisFileName', default="helvetas_data/Export_IP_Reporting_ASAnalysis_Synopsis.xlsx")
# parser.add_argument('--yearlyProgressFileList', nargs='*', default = yearlyProgressFileList)
# parser.add_argument('--sampleOutputDataFile', default="helvetas_data/Hack4Good Helvetas Output(1).xlsx")
# parser.add_argument('--translate', action='store_true')
# parser.add_argument('--lemmatizedDataFile', nargs='*', default='')
# parser.add_argument('--outputFileName', default="outputDataHelvetas.xlsx")
# args = parser.parse_args()

# # load spacey english
# nlp_en = spacy.load("en_core_web_lg")

# # load WF names
# class_labels_df = pd.read_csv("helvetas_data/helvetas_WF_names.csv")

# # class labels
# class_labels_en = list(class_labels_df["WF Names EN"])

# # load excel files
# synopsisFileName = args.synopsisFileName
# yearlyProgressFileList = args.yearlyProgressFileList
# sampleOutputDataFile = args.sampleOutputDataFile

# print("Loading Helvetas data from files")
# helvetasDataObject = HelvetasData(synopsisFileName, yearlyProgressFileList, sampleOutputDataFile)
# outputDF = helvetasDataObject.outputDataFrame
# lemmaDF = []
# dataIsLemmatized = False
# if args.lemmatizedDataFile != '':
#     dataIsLemmatized = True
#     if len(args.lemmatizedDataFile) == 2:
#         print("Loading an already lemmatized data file from: %s and sheet %s" % (args.lemmatizedDataFile[0], args.lemmatizedDataFile[1]))
#         helvetasDataObject.loadLemmatizedData(args.lemmatizedDataFile[0], args.lemmatizedDataFile[1])
#     else:
#         print("Loading an already lemmatized data file from: %s" % (args.lemmatizedDataFile[0]))
#         helvetasDataObject.loadLemmatizedData(args.lemmatizedDataFile[0], "")        

# # Translate data step (is skipped if data is already translated)
# vtranslate=np.vectorize(ts.translate)

# if args.translate is True and dataIsLemmatized is False:
#     translatedDF = outputDF.copy(deep=True)
#     print("Translating data into English, this may take a while...")
#     translatedDF["Outcome 1"] = vtranslate(translatedDF["Outcome 1"])
#     print("Translating outcome 1")
#     translatedDF["Description"] = vtranslate(translatedDF["Description"])
#     print("Translating outcome 2")
#     translatedDF["Outcome 2"] = vtranslate(translatedDF["Outcome 2"])
#     print("Translating outcome 4")
#     translatedDF["Outcome 4"] = vtranslate(translatedDF["Outcome 4"])
#     print("Translating outcome 5")
#     translatedDF["Outcome 5"] = vtranslate(translatedDF["Outcome 5"])
#     helvetasDataObject.setTranslatedDataFrame(translatedDF)
#     lemmaDF = translatedDF.copy(deep=True)
# else:
#     print("Not translating data, output is either already translated or is in original language.")
#     lemmaDF = outputDF.copy(deep=True)


# # Lemmatize data step (is skipped if lemmatized data is loaded in)
# if dataIsLemmatized is False:
#     print("Lemmatizing the data... this could take a while...")
#     lemmaDF["Description"] = pp.standardize(lemmaDF["Description"].astype(str), nlp_en)
#     lemmaDF["Outcome 1"] = pp.standardize(lemmaDF["Outcome 1"].astype(str), nlp_en)
#     lemmaDF["Outcome 2"] = pp.standardize(lemmaDF["Outcome 2"].astype(str), nlp_en)
#     lemmaDF["Outcome 4"] = pp.standardize(lemmaDF["Outcome 4"].astype(str), nlp_en)
#     lemmaDF["Outcome 5"] = pp.standardize(lemmaDF["Outcome 5"].astype(str), nlp_en)
#     # lemmaDF["Outcome 1"] = np.vectorize(pp.standardize)(lemmaDF["Outcome 1"])
#     # lemmaDF["Outcome 2"] = np.vectorize(pp.standardize)(lemmaDF["Outcome 2"])
#     # lemmaDF["Outcome 4"] = np.vectorize(pp.standardize)(lemmaDF["Outcome 4"])
#     # lemmaDF["Outcome 5"] = np.vectorize(pp.standardize)(lemmaDF["Outcome 5"])
#     helvetasDataObject.setLemmatizedDataFrame(lemmaDF)
#     print("Finished Lemmatizing!")

# # print(type(lemmaDF["Description"][0]))
# # add processing algorithms here, with helvetasDataObject.lemmatizedDataFrame having already lemmatized and translated data.
# # Create Dictionary of ALL project descriptions 

# lemmData = helvetasDataObject.lemmatizedDataFrame
# # print(lemmData["Description"][0])
# # data = [text.strip('][').replace("'", "").split(', ') for text in lemmData["Description"]]

# # data = [word for words in lemmData["Description"] for word in words]
# data = lemmData["Description"]
# # print(data)
# id2word = corpora.Dictionary(data)

# # Create Corpus
# print("Building corpus...")
# texts = lemmData["Description"].array

# # Term Document Frequency
# corpus = [id2word.doc2bow(text) for text in data]
# print("Running LDA model to generate and assign accents")
# wordList, result = run_LDA(id2word, corpus)

# # print(wordList)



# helvetasDataObject.outputDataFrame["Accent 1"] = result["Topic 0"]
# helvetasDataObject.outputDataFrame["Accent 2"] = result["Topic 1"]
# helvetasDataObject.outputDataFrame["Accent 3"] = result["Topic 2"]
# helvetasDataObject.outputDataFrame["Accent 4"] = result["Topic 3"]
# helvetasDataObject.outputDataFrame["Accent 5"] = result["Topic 4"]
# helvetasDataObject.outputDataFrame["Accent 6"] = result["Topic 5"]
# helvetasDataObject.outputDataFrame["Accent 7"] = result["Topic 6"]
# helvetasDataObject.outputDataFrame["Accent 8"] = result["Topic 7"]
# helvetasDataObject.outputDataFrame["Accent 9"] = result["Topic 8"]
# helvetasDataObject.outputDataFrame["Accent 10"] = result["Topic 9"]

# # build topic dictionary
# dictDF = pd.DataFrame(columns=['Accent 1', 'Accent 2','Accent 3','Accent 4','Accent 5','Accent 6','Accent 7','Accent 8','Accent 9', 'Accent 10',])
# dictDF["Accent 1"] = wordList[0]
# dictDF["Accent 2"] = wordList[1]
# dictDF["Accent 3"] = wordList[2]
# dictDF["Accent 4"] = wordList[3]
# dictDF["Accent 5"] = wordList[4]
# dictDF["Accent 6"] = wordList[5]
# dictDF["Accent 7"] = wordList[6]
# dictDF["Accent 8"] = wordList[7]
# dictDF["Accent 9"] = wordList[8]
# dictDF["Accent 10"] = wordList[9]
# # print(dictDF)
# helvetasDataObject.accentDict = dictDF

# # create model WF classification model
# print("Building embedding classifier model...")
# EC_en_euclid = models.EmbeddingClassifier(class_labels_en, nlp_en)

# # predict WF
# print("Generating predictions....")
# wfOutcome1Prediction = EC_en_euclid.predict(helvetasDataObject.lemmatizedDataFrame["Outcome 1"])
# wfOutcome2Prediction = EC_en_euclid.predict(helvetasDataObject.lemmatizedDataFrame["Outcome 2"])
# wfOutcome4Prediction = EC_en_euclid.predict(helvetasDataObject.lemmatizedDataFrame["Outcome 4"])
# wfOutcome5Prediction = EC_en_euclid.predict(helvetasDataObject.lemmatizedDataFrame["Outcome 5"])
# # add formatted data
# helvetasDataObject.outputDataFrame["WF Outcome 1"] = wfOutcome1Prediction
# helvetasDataObject.outputDataFrame["WF Outcome 2"] = wfOutcome2Prediction
# helvetasDataObject.outputDataFrame["WF Outcome 3"] = wfOutcome4Prediction
# helvetasDataObject.outputDataFrame["WF Outcome 4"] = wfOutcome5Prediction

# # put into lemmatized sheet
# helvetasDataObject.lemmatizedDataFrame["WF Outcome 1"] = wfOutcome1Prediction
# helvetasDataObject.lemmatizedDataFrame["WF Outcome 2"] = wfOutcome2Prediction
# helvetasDataObject.lemmatizedDataFrame["WF Outcome 3"] = wfOutcome4Prediction
# helvetasDataObject.lemmatizedDataFrame["WF Outcome 4"] = wfOutcome5Prediction


# # save to file
# print("Saving to file...")
# helvetasDataObject.toFile(args.outputFileName)





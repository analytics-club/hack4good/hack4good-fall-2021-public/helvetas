# Helvetas Hack4Good 2021 Project
### Annual Project Synopsis and Classification
The yearly reporting of Helvetas’ projects that spread across 30 different countries is crucial to analyse the progress, impact and success in each area of activities. In order to improve reliability and timeliness of data, to increase efficiency and depth in data collection, management and analysis and to increase availability of high quality information for steering and accountability, the reports are now required to be automatically classified. Having as an input a set of organization-wide Performance Indicators, Project Synthesis and Summary Sheets and Annual reports for 300 projects per year for 5 years, each project has to be assigned to the different thematic areas of interest and made available for different users according to the semantic information which it contains.

***

[[_TOC_]]


## Important links

<div align="center"><p>
<a href="https://trello.com/h4g2021teamhelvetas">Trello Workspace</a> • 
<a href="https://drive.google.com/drive/u/3/folders/1StZKFa5XAEIVawvAclxCsjMYxrHBHqVJ">Google Drive</a> • 
<a href="https://polybox.ethz.ch/index.php/f/2556779668">Polybox</a> • 
<a href="https://gitlab.com/analytics-club/hack4good/hack4good-fall-2021/helvetas">GitLab</a> • 
<a href="https://drive.google.com/file/d/1246VqPBU54GmAGsO_M412JXpAZeA53FU/view?usp=sharing">Welcome Booklet</a> • 
<a href="https://docs.google.com/document/d/13DUhZcPjsDLuPY5vn1hAbDfcAirIPXZW">Project Description</a>
</p></div>

<div align="center"><p>
<a href="https://spacy.io/api">spaCy Docs</a> • 
<a href="https://numpy.org/doc/1.21/">Numpy Docs</a> • 
<a href="https://pandas.pydata.org/docs/">Pandas Docs</a> • 
<a href="https://www.nltk.org/">NLPTK Docs</a> • 
<a href="https://matplotlib.org/stable/contents.html">Matplotlib Docs</a> • 
<a href="https://docs.pytest.org/en/6.2.x/">PyTest Docs</a>
</p></div>


## Team

**H4G project responsible:** Mariana (mariana.coelho@analytics-club.org)  
**NGO representative:** Paulo Rodrigues (paulo.rodrigues@helvetas.org)  
**Mentor:** Marco Mancini (mancini.mm95@gmail.com)
  
**Team members:**  
- Catalina Dragusin
- David Simon Tetruashvili
- Jackson Stanhope
- Tom Haidinger

## Getting Started

### Cloning the Repository

Please use `git` to clone the repository:

```bash
$ git clone git@gitlab.com:analytics-club/hack4good/hack4good-fall-2021/helvetas/helvetas.git
$ cd helvetas
```

### Conda Environment

<sup>(Sorry, Marco.)</sup>

#### Installing and Updating


We decided to rely on using Conda python environments for their ease of use.

You can *install* OR *update* the `h4g` conda environment by using our `setup.sh` script:

```bash
$ cd helvetas
$ .scripts/setup.sh
```

Activate the environment using

```bash
$ conda activate h4g
```

and deactivate via

```bash
$ conda deactivate
```

If you so wish, you may also source `activate.sh` within your own scripts:

```bash
$ source .scripts/activate.sh
```

The environment contains modules such as `numpy`, `pandas`, `spacy`, and `nlptk`.

**Note:** Once you have the environment the `setup.sh` script should also install the spaCy English pipeline. If it doesn't, use the following command to install it:

```bash
$ python -m spacy download en_core_web_trf
```

Adding new modules is simple with

```bash
$ conda install module
```
or

```bash
$ conda install -c conda-forge module
```

#### Adding New Packages

If you add new modules (either via `pip` or `conda`) the only thing you need to do is update the `env.yaml` file to describe your current environment.

**BE CAREFUL**: If you change the `env.yaml` file, you are changing the environemnt REPOSITORY-WIDE!
If you need to experiment with a module, first make a clone of the original "h4g" environment and do it there. One you decide that the dependancy is good to use you can add 

*The `setup.sh` script WILL NOT REMOVE modules from your local environment, just add them. This should be safe guard against breaking dependancies, but will make the environment only grow. If you need to remove unneeded dependancies use the `--prune` flag with the command inside of `setup.sh` (or google it).
## Repository House-keeping

### Commit Messages

Please use consistant commmit messages (preferably in the form presented by Tim Pope in [his blog post](https://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html)).

In addition we thought that it would be fun to use the [Gitmojis](https://gitmoji.dev/) tool to spice the messages up.
You can install the Gitmojis CLI with `npm` or (if you're on Mac or Linux) `brew`:

```bash
$ npm i -g gitmoji-cli
```

```bash
$ brew install gitmoji
```
After installing you can either use the `gitmoji -c` command to commit via the CLI or use `gitmoji -i` in the repository directory to create a Gitmoji commit hook.

If you choose to do the latter then `gitmoji -c` will be called each time you use `git commit` in this repository (without the `-m` flag).

### Branch Names and Pull Requests

- Please use branches and don't work on `main`. 
- Use underscores to name banches. 
- Name branches using the topic you're working on (e.g., `preprocessing`).
- Don't `git merge` to `main`. Use a pull request in GitLab so we can perform end-of-sprint code reviews.

## Testing

The project uses `pytest` as its unit testing framework and follows test-driven development. To run the tests use the `test.sh` script:

```bash
$ .scripts/test.sh
```
